#!/bin/bash

X=$(pwd)

WP="$" 
Bracket="{" 
Name="IP_WP_" 
Db="DB_NAME"
Root="DB_ROOT_PASSWORD"
Num="2" 
EndBracket="}"

rm -rf "$X/docker-compose-dev.yml"

echo "version: '3'" | tee -a docker-compose-dev.yml

echo "services:"  | tee -a docker-compose-dev.yml

for i in 1 2 3 4 5

do	

  echo " pma-$i:"
  echo "   image: phpmyadmin/phpmyadmin"
  echo "   environment:"
  echo "     PMA_HOST: db-$i"
  echo "     PMA_PORT: 3306"
  echo "     MYSQL_ROOT_PASSWORD: "${DB_ROOT_PASSWORD}""
  echo "   ports:"
  echo "     - $WP$Bracket$Name$i$EndBracket:8080:80"
  echo "   links:"
  echo "     - db-$i:db-$i"


  echo " db-$i:"
  echo "   image: mysql:latest"
  echo "   ports:"
  echo "    - $WP$Bracket$Name$i$EndBracket:3306:3306 # change ip if required"
  echo "   command: ["
  echo "       '--default_authentication_plugin=mysql_native_password',"
  echo "       '--character-set-server=utf8mb4',"
  echo "       '--collation-server=utf8mb4_unicode_ci'"
  echo "   ]"
  echo "   volumes:"
  echo "     - ./wp-data-$i:/docker-entrypoint-initdb.d"
  echo "     - ./wp-db-$i:/var/lib/mysql"
  echo "   environment:"
  echo "     MYSQL_DATABASE: "$WP$Bracket$Db$EndBracket""
  echo "     MYSQL_ROOT_PASSWORD: "$WP$Bracket$Root$EndBracket""


  echo " wp-$i:"
  echo "   image: wordpress:latest"
  echo "   ports:"
  echo "     - $WP$Bracket$Name$i$EndBracket:80:80 # change ip if required"
  echo "   volumes:"
  echo "     - ./config-$i/php.conf.ini:/usr/local/etc/php/conf.d/conf.ini"
  echo "     - ./wp-app-$i:/var/www/html # Full wordpress project"
  echo "     #- ./plugin-name/trunk/:/var/www/html/wp-content/plugins/plugin-name # Plugin development"
  echo "     #- ./theme-name/trunk/:/var/www/html/wp-content/themes/theme-name # Theme development"
  echo "   environment:"
  echo "     WORDPRESS_DB_HOST: db-$i"
  echo "     WORDPRESS_DB_NAME: "$WP$Bracket$Db$EndBracket""
  echo "     WORDPRESS_DB_USER: root"
  echo "     WORDPRESS_DB_PASSWORD: "$WP$Bracket$Root$EndBracket""
  echo "   depends_on:"
  echo "     - db-$i" 
  echo "   links:"
  echo "     - db-$i" 


done 2>&1 | tee -a docker-compose-dev.yml


rm -rf "$X/.dev.env"

echo "DB_ROOT_PASSWORD=password" | tee .dev.env

echo "DB_NAME=wordpress" | tee -a .dev.env 

for i in 1 2 3 4 5

do 

echo "$Name$i=127.0.0.$i"

done 2>&1 | tee -a .dev.env

