
Setting up a Wordpress Install and a basic php image hosted different ips and ports

```
http://127.0.0.1:80/     => Wordpress
http://127.0.0.1:8080/   => PhpMyAdmin
http://127.0.0.149:80/   => Wordpress
http://127.0.0.149:8080/ => PhpMyAdmin
http://127.0.0.1:5002/   => PHP
http://127.0.0.2:5003/   => PHP
http://127.0.0.150:5004/ => PHP
```

Range (1-150) works. 

Next is to set a second wordpress instance live. - Check  
And begin work on the BlackBox tunnel  


phpMyAdmin

```
root
password
```


```
mv ./.dev.env ./.env
mv ./docker-compose-dev.yml ./docker-compose.yml
docker-compose up
```


Multiple Ports with `.Env` variables predefined from `docker-compose-builder` script 

```
127.0.0.1:8080->80/tcp                blackbox-mini_pma-1_1
127.0.0.1:80->80/tcp                  blackbox-mini_wp-1_1
127.0.0.5:80->80/tcp                  blackbox-mini_wp-5_1
127.0.0.5:8080->80/tcp                blackbox-mini_pma-5_1
127.0.0.4:80->80/tcp                  blackbox-mini_wp-4_1
127.0.0.4:8080->80/tcp                blackbox-mini_pma-4_1
127.0.0.2:8080->80/tcp                blackbox-mini_pma-2_1
127.0.0.2:80->80/tcp                  blackbox-mini_wp-2_1
127.0.0.3:80->80/tcp                  blackbox-mini_wp-3_1
127.0.0.3:8080->80/tcp                blackbox-mini_pma-3_1
127.0.0.3:3306->3306/tcp, 33060/tcp   blackbox-mini_db-3_1
127.0.0.2:3306->3306/tcp, 33060/tcp   blackbox-mini_db-2_1
127.0.0.4:3306->3306/tcp, 33060/tcp   blackbox-mini_db-4_1
127.0.0.5:3306->3306/tcp, 33060/tcp   blackbox-mini_db-5_1
127.0.0.1:3306->3306/tcp, 33060/tcp   blackbox-mini_db-1_1
```


